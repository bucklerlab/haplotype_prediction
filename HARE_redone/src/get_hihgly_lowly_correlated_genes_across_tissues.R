#------------------------------------------------------- 
#get pathways enriched in highly and lowly correlated genes across tissues
#-------------------------------------------------------
#libraries
library(data.table)
library(foreach)
library(dplyr)
library(doParallel) #run foreach in parallel
library(ggplot2) 
library(tidyr) #wide-long data conversion
registerDoParallel(cores = 10)

# if (!requireNamespace("BiocManager", quietly=TRUE)) install.packages("BiocManager")
# BiocManager::install("topGO")
# 
# BiocManager::install("Biostrings")
# BiocManager::install("biomaRt")

#plot the distribution of correlation across all tissues: 21 total
# # for imputed expression
tissues <- c('GRoot_GShoot', 'GRoot_L3Tip', 'GRoot_L3Base', 'GRoot_LMAN', 'GRoot_LMAD', 'GRoot_Kern',
             'GShoot_L3Tip', 'GShoot_L3Base', 'GShoot_LMAN', 'GShoot_LMAD', 'GShoot_Kern', 
             'L3Tip_L3Base', 'L3Tip_LMAN', 'L3Tip_LMAD', 'L3Tip_Kern',
             'L3Base_LMAN', 'L3Base_LMAD', 'L3Base_Kern',
             'LMAN_LMAD', 'LMAN_Kern',
             'LMAD_Kern'
)

method_list <- c('meas', 'cis_trans', 'cis_random', 'cis_fixed')[1:2] #remove files with tissues_imp from the folder

data_dir <- '/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/corrlation_across_tissues'
plot_dir <- '/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/plots_manuscript'

#read all files for tissue combination and imputation and measured and rbind them
df_combined <- foreach(tissue_comb = tissues, .combine = rbind) %do% {
  
  df_method <- foreach(exp = method_list, .combine = rbind) %do% {
    
    temp_df <- read.csv(paste0(data_dir, "/", tissue_comb,"_", exp, ".csv"))
    
    colnames(temp_df)[colnames(temp_df) == tissue_comb] <- "tissue_cor"
    
    temp_df$tissue <- tissue_comb
    
    temp_df$expression <- exp
    
    #message("done with exp ", exp)
    
    return(temp_df)
    
  }
  message("done with tissue_comb ", tissue_comb)
  
  return(df_method)
  
}

#imputed exp
exp <- "cis_trans"
df_exp <- df_combined %>% filter(expression == exp)
cor_high_imp <- df_exp %>% filter(tissue_cor > 0.75)

cor_low_imp <- df_exp %>% filter(tissue_cor < 0.12 & tissue_cor > -0.12)

common_genes <- intersect(unique(cor_low_imp$X), unique(cor_high_imp$X))
high_common_genes <- df_exp %>% filter(X %in% common_genes) %>% filter(tissue_cor > 0.75)
write.csv(cor_low_imp, "/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/GO_pathway_analysis/low_cor_imputed_05.csv")
write.csv(cor_low_imp, "/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/GO_pathway_analysis/low_cor_imputed_75.csv")

#get a list of genes for predicting traits to look transferrability for predicting
#since same genes are in high cor in one tissue combination and low in other, to get rid of confound pick one comb and work 

tissue_comb <- "GRoot_Kern"
low_genes1 <- cor_low_imp %>% filter(tissue == tissue_comb) 
high_genes1 <- cor_high_imp %>% filter(tissue == tissue_comb)

#-------------------------------------------------------
#explore these two groups
#get expression values in these genes 
tissue <- "LMAN"
exp <- fread(paste0("/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/data/predicted_exp_", tissue, "_re_2.csv"))

low_exp <- exp %>% select(gene, cis_trans) %>%
  filter(gene %in% as.character(unique(cor_low_imp$X))) %>% 
  filter(!gene %in% common_genes) %>% 
  filter(cis_trans>5)

low_exp_all <- exp %>% select(gene, cis_trans) %>%
  filter(gene %in% as.character(unique(cor_low_imp$X))) %>% 
  filter(!gene %in% common_genes)

high_exp <- exp %>% select(gene, cis_trans) %>% 
  filter(gene %in% as.character(unique(cor_high_imp$X))) %>% 
  filter(!gene %in% common_genes) %>%
  filter(cis_trans>5)

high_exp_all <- exp %>% select(gene, cis_trans) %>% 
  filter(gene %in% as.character(unique(cor_high_imp$X))) %>% 
  filter(!gene %in% common_genes)

ggplot() + 
  geom_histogram(data = low_exp, aes(x = cis_trans), binwidth = 10, fill = "blue") + 
  labs(x = "expression in low correlated genes", title = paste0(tissue)) +
  xlim(0, 2000) + ylim(0, 1500)
ggplot() + 
  geom_histogram(data = high_exp, aes(x = cis_trans), binwidth = 10, fill = "blue") + 
  labs(x = "expression in highly correlated genes", title = paste0(tissue)) +
  xlim(5, 2000) + ylim(0, 1500)

###################tissue specificity 
#Function require a vector with expression of one gene in different tissues.
#If expression for one tissue is not known, gene specificity for this gene is NA
#Minimum 2 tissues

fTau <- function(x)
{
  if(all(!is.na(x)))
  {
    if(min(x, na.rm=TRUE) >= 0)
    {
      if(max(x)!=0)
      {
        x <- (1-(x/max(x)))
        res <- sum(x, na.rm=TRUE)
        res <- res/(length(x)-1)
      } else {
        res <- 0
      }
    } else {
      res <- NA
      #print("Expression values have to be positive!")
    } 
  } else {
    res <- NA
    #print("No data for this gene avalable.")
  } 
  return(res)
}

meas_exp <- fread("/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/expression/data/df_STAR_HTSeq_counts_B73_match_based_on_genet_dist_DESeq2_normed_fpm_rounded_avg_multiple_exp_values.csv")

taxa <- "CML108"

exp_taxa <- meas_exp[taxa_GBS == taxa,]

exp_taxa_filt <- exp_taxa[, colSums(exp_taxa[, 3:ncol(exp_taxa)]) != 0, with = F]
exp_taxa_filt1 <- exp_taxa[, colSums(exp_taxa[, 3:10]) != 0, with = F]
#before gettin tau metrics filter for set of genes that have normal distribution

sig_genes <- foreach(genes = colnames(exp_taxa_filt)[3:100], .combine = rbind) %do% {
  test <- shapiro.test(exp_taxa_filt[, ..genes])
  p_val <- test$p.value
  
  
}
     
     test <- shapiro.test(exp_taxa_filt$AC148152.3_FG005)
test$p.value



tau_metrics <- foreach(i = 3:ncol(exp_taxa_filt), .combine = rbind) %do%
  {
    genes <- unlist(exp_taxa_filt[, i, with = F])
    out <- fTau(genes)
    return(out)
  }

tau_metrics_df <- data.frame(tau_metrics)

ggplot(data = tau_metrics_df, aes(x = tau_metrics)) + geom_histogram() + 
  xlab("tissue specificity using tau metrics")

imp_exp <- fread("/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/data/exp_matrix_minreads1/exp_282_GRoot_cis_only_model_minreads1.csv")

filelist <- list.files(path = "/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/data/exp_matrix_minreads0", pattern = glob2rx("*282*cis_trans_model.csv"), full.names = TRUE)[1:7]

taxa_1 <- "A188"
exp_imp <- foreach(files = filelist) %do% {
  
  df_temp <- fread(files)
  df_taxa <- df_temp[taxa == taxa_1,]
  return(df_taxa)
}

common_genes <- Reduce(intersect, list(colnames(exp_imp[[1]]), colnames(exp_imp[[2]]), colnames(exp_imp[[3]]), colnames(exp_imp[[4]]), 
                          colnames(exp_imp[[5]]), colnames(exp_imp[[6]]), colnames(exp_imp[[7]])))

exp_imp_taxa <- rbind(exp_imp[[1]][, ..common_genes], exp_imp[[2]][, ..common_genes], exp_imp[[3]][, ..common_genes], exp_imp[[4]][, ..common_genes], 
                      exp_imp[[5]][, ..common_genes], exp_imp[[6]][, ..common_genes], exp_imp[[7]][, ..common_genes])


tau_metrics <- foreach(i = 1:17170, .combine = rbind) %do%
  {
    genes <- unlist(exp_imp_taxa[, i, with = F])
    out <- fTau(genes)
    return(out)
  }

tau_metrics_df <- data.frame(tau_metrics)

ggplot(data = tau_metrics_df, aes(x = tau_metrics)) + geom_histogram() + 
  xlab("tissue specificity using tau metrics")


exp <- "meas"
cor_high_meas <- df_combined %>% filter(tissue_cor > 0.5, expression == exp)
cor_low_meas <- df_combined %>% filter(expression ==exp) %>% filter(tissue_cor < 0.001, tissue_cor > -0.001)

cor_high_common <- intersect(unique(cor_high_imp$X), unique(cor_high_meas$X))
cor_low_common <- intersect(unique(cor_low_imp$X), unique(cor_low_meas$X))

#GO annotations
getGOInfo(gene_list_high)

mart <- useMart('ENSEMBL_MART_ENSEMBL')
listDatasets(mart)

#kegg genes
kegg <- fread('/Users/ag2484-admin/Dropbox/bucklerlab/heckathon/heckathon_sep2020/all_v4_genes_in_Kegg_Pathways.txt')


#------------------------------------------------------- 
#check if the distribution of genes doesn't look weird
#-------------------------------------------------------
filelist <- list.files(path = "/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/data/exp_matrix_minreads0", pattern = glob2rx("*282*cis_trans_model.csv"), full.names = TRUE)[1:7]

df_exp <- fread(file = filelist[1])

exp_imp <- foreach(files = filelist[1]) %do% {
  
  df_temp <- fread(files)
  p_val <- foreach(genes = colnames(df_temp)[-1], .combine = rbind) %dopar% {
    p_val_temp <- try(shapiro.test(unlist(df_exp[, genes, with = FALSE]))$p.value)
    return(p_val_temp)
  }
  return(p_val)
}

p_val_all <- rbind.data.frame(exp_imp)
