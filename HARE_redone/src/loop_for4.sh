#! /bin/bash

#run script 4_random in parallel for only 50 randomizations at a time

VAR1="101 151"
VAR2="150 200"

tissue='GRoot'
fun()
{
    set $VAR2
    for i in $VAR1; do
        echo start with "$i"
        Rscript --vanilla 4b_phenotype_prediction_outofpopulation_random.R "$tissue" "$i" "$1"
        echo done with "$i"
        shift
    done
}

fun
