
#------------------------------------------------------- 
#plot correlation distribution between measured and observed expression (282 only to compare with measured)
#-------------------------------------------------------

#libraries
library(foreach)
library(patchwork) #for plotting multiple plots
library(ggplot2) 
library(tidyr) #wide-long data conversion

plot_dir <- '/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/plots_manuscript'
data_dir <- '/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/correlation_within_tissue'
tissues <- c('L3Tip', 'L3Base','LMAD', 'LMAN' , 'GShoot', 'GRoot', 'Kern')

#function to combine all tissue from each method of expresison imputation
get_correlation_df <- function(filelist) {
  cor_list <- foreach(datafile = filelist) %do% 
    {
      temp_df <- read.csv(datafile)
      return(temp_df)
    }
  
  cor_out <- Reduce(function(...) merge(..., by="X", all = T), lapply(cor_list, function(x) data.frame(x)))
  
  return(cor_out)
}

#get three data frames for each expression imputation method
#cis_trans
exp <- 'cis_trans'
exp_rename <- "cis random (accounting for trans)"
files <- list.files(data_dir, pattern = exp, full.names = TRUE)

cis_trans <- get_correlation_df(filelist = files)
cis_trans[, 'model'] <- exp_rename

#cis_random

exp <- 'cis_random'
exp_rename <- "cis only random"
files <- list.files(data_dir, pattern = exp, full.names = TRUE)

cis_random <- get_correlation_df(filelist = files)
cis_random[, 'model'] <- exp_rename

#cis_fixed
exp <- 'cis_fixed'
exp_rename <- "cis only fixed"
files <- list.files(data_dir, pattern = exp, full.names = TRUE)

cis_fixed <- get_correlation_df(filelist = files)
cis_fixed[, 'model'] <- exp_rename

#get plots of each tissue
cor_alltissues <- rbind(cis_fixed, cis_trans, cis_random)

cor_alltissues.long <- gather(cor_alltissues, tissue, correlation, GRoot:LMAN)

within_tissue_cor <- ggplot(data = cor_alltissues.long, aes(correlation, fill = model)) +
  geom_histogram(aes(y = ..density..),  alpha = 0.5, bins = 100, position = 'identity') + 
  theme_bw() + 
  facet_wrap(~tissue, ncol = 2) +
  labs(x = "correlation between measured and haplotype imputed expression in Goodman panel") +
  theme(legend.justification = c(1, 0), legend.position = c(0.9, 0.01)) +
  theme(axis.title = element_text(color = "black", size = 14, face = "bold"),
        plot.title = element_text(color="blue", size=16, face="bold.italic"),
        axis.text = element_text(color = "grey20", size = 12),
        strip.text = element_text(size = 12, face = "bold"),
        legend.text=element_text(size=12))

ggsave(paste0(plot_dir, "/cor_dist_meas_imp_expression_within_tissue.png"), within_tissue_cor, width = 10, height = 6, dpi = 'retina' )
