

###packages required

library(data.table)
library(glmnet)
library(glmnetUtils)
library(dplyr)
library(doParallel)
library(foreach)
registerDoParallel(cores = 10)

args <- commandArgs(trailingOnly=TRUE) #runfrom command line with args: (tissue, rand1, rand_n)

#functions
predictPhenotype_withinpop <- function(data,
                                       seed,
                                       sampleProp,
                                       nFolds,
                                       trait,
                                       ...) {
  
  # Split data into training and testing components (using seed for repeatibility)
  set.seed(seed = seed)
  message("trait in function ", trait)
  split <- sample(
    x    = nrow(data),
    size = floor(sampleProp * nrow(data))
  )
  train <- data[split, ]
  test  <- data[-split, ]
  
  # transform both sets into matrices using `data.matrix`
  train_mat <- data.matrix(train[, 3:ncol(data)])
  test_mat  <- data.matrix(test[, 3:ncol(data)])
  
  # Model the matrices using glmnet
  cv <- glmnet::cv.glmnet(
    x      = train_mat,
    y      = train[[trait]],
    alpha  = 0,
    nfolds = nFolds
  )
  #fit model with optimum lambda
  fit <- glmnet::glmnet(
    x      = train_mat,
    y      = train[[trait]],
    alpha  = 0,
    lambda = cv$lambda.min, #10^seq(4, -3, length = 100), # grid
    thresh = 1e-12
  )
  # Predict
  pred <- predict(
    object = fit,
    newx   = test_mat,
    type   = "response",
    s      = cv$lambda.min
  )
  
  # Phenotype class / data frame example
  return(
    data.frame(
      name = test[,1],
      predicted = pred,
      observed = test[,2]
    )
  )
  #return(round(cor(pred, test[,2])))
}

pred_acc <- function(data){
  #t_df <- data %>% filter(X1 > 1 & X1 < 2000)
  cor_value <- round(cor(data[,2], data[,3]), 3)
  return(cor_value)
}

error <- function(obs, pred){
  temp_df2 <- data.frame(obs, pred) %>% na.omit()
  
  rmse <- sqrt(sum((temp_df2$pred-temp_df2$obs)^2)/nrow(temp_df2))
  mae <- sum(abs(temp_df2$pred-temp_df2$obs))/nrow(temp_df2)
  return(c(rmse, mae))
}

#code starts here
output_dir <- '/Volumes/bucklerlab_backup/haplotype_prediction/HARE_redone/results/withinpopulation'
geno_dir <- '/Volumes/bucklerlab_backup/haplotype_prediction/HARE_redone/data/exp_random_re'
pheno_282 <- fread("/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/phenotype_prediction/data/all_Assoc282_Phenos.csv")

# output_dir <- '/workdir/ag2484/results'
# geno_dir <- '/workdir/ag2484/data'
# pheno_282 <- fread("/workdir/ag2484/data/all_Assoc282_Phenos.csv")

#parameters
#how <- c('mean', 'max')[2]
how <- args[1]
print(how)

#exp_value <- c('cis_trans', 'cis_random', 'cis_fixed')[1] or 'measured_exp'
exp_value <- 'cis_trans'
print(exp_value)

cross_val <- 5 #20 randomsamplle of test set

selectedtraits <- c("GDD_DTS_BLUP;Peiffer_2014", "GDD_DTA_BLUP;Peiffer_2014", "GDD_ASI_BLUP;Peiffer_2014", "plant_height_BLUP;Peiffer_2014",
                    "ear_height_BLUP;Peiffer_2014", "leaflength_BLUP;Hung_2012_1", "leafwidth_BLUP;Hung_2012_1", "tassprimbranchno_BLUP;Hung_2012_1",
                    "tasslength_BLUP;Hung_2012_1", "upperleafangle_BLUP;Hung_2012_1", "cobdiam_BLUP;Hung_2012_1", "coblength_BLUP;Hung_2012_1",
                    "earrowno_BLUP;Hung_2012_1", "kernelnoperrow_BLUP;Hung_2012_1", "earmass_BLUP;Hung_2012_1", "cobmass_BLUP;Hung_2012_1",
                    "totalkernelweight_BLUP;Hung_2012_1",  "weight20kernels_BLUP;Hung_2012_1", "totalkernelno_BLUP;Hung_2012_1", "nodenumberbelowear_BLUP;Hung_2012_1",
                    "nodenumberaboveear_BLUP;Hung_2012_1", "numbraceroots_BLUP;Hung_2012_1", "southern_leaf_blight_BLUP;Kump_2011", "starch_kernel_BLUP;Cook_2012",
                    "protein_kernel_BLUP;Cook_2012", "oil_kernel_BLUP;Cook_2012")


pheno_282 <- pheno_282[, c('Entry_ID_noSpaces', selectedtraits), with = FALSE]
setnames(pheno_282, 'Entry_ID_noSpaces', 'taxa')

##for imputed expression
n_seed <- args[3]
s_seed <- args[2]
traits <- colnames(pheno_282)[-1]

message('starting loop')

traits <- traits[1:5]
#for (sed in s_seed:n_seed){
foreach(sed = s_seed:n_seed) %dopar% {
  geno_282_all <- readRDS(paste0(geno_dir, '/exp_282_rand_', sed, '_', how, '_', exp_value, '_model.RDS'))
  geno_282 <- Filter(function(x) !all(is.na(x)), geno_282_all)
  setnames(geno_282, 'V1', 'taxa')
  geno_282$taxa <- tolower(geno_282$taxa)
  
  
  # acc_trait <- c()
  # for (trait in traits) {
  acc_trait <- foreach(trait = traits, .combine = rbind) %do% {
    message("working on trait ", trait)
    join_data <- merge(pheno_282[, c('taxa', trait), with = FALSE], geno_282, by = 'taxa')
    join_data_wo_na <- join_data %>% na.omit()
    
    # acc_seed <- foreach (i=1:cross_val, .combine = cbind) %do% {
    acc_seed <- c()
      for (i in 1:cross_val){
        message("starting cross-val ", i)
      predictPhenotype_df <- predictPhenotype_withinpop(data = join_data_wo_na, seed = i, sampleProp = 0.8, nFolds = 10, trait = trait)
      message('done with cross-val ', i)
      write.csv(predictPhenotype_df, paste0(output_dir, '/', how, '/predicted_values/', trait, '_', how, '_', sed, exp_value, '_', i, '.csv'))
      
      pred_acc_cross_val <- pred_acc(predictPhenotype_df)
      
      acc_seed <- cbind(acc_seed, pred_acc_cross_val)
      # return(pred_acc_cross_val)
    }
    
    message("done with trait ", trait)
    rownames(acc_seed) <- trait
    
    #acc_trait <- rbind(acc_trait, acc_seed)
    return(acc_seed)
    
  }
  
  write.csv(acc_trait, paste0(output_dir, '/', how, '/', how, '_', sed, '_', exp_value, '_alltraits.csv'))
}


