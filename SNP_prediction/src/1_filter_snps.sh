#!/bin/bash

# ---------------------------------------------------------------
# Author: Anju Giri (ag2484@cornell.edu)

# Objectives:
#   1. Get SNPs from HARE regions
#   2. Filter HARE for allele frequency, indels

# Input files Required:
#   1. Output vcf from 0_get_snps_from_db_{282/NAM}.sh
#   2. Bed file from HARE genes in v3
# ---------------------------------------------------------------


# -----------------------------------------
#  Get SNPs from HARE regions
# -----------------------------------------
/workdir/ag2484/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/ag2484/nam_hare_downsample_debug.log \
    -Xmx1000g \
    -maxThreads 86 \
    -importGuess /workdir/ag2484/NAM_phg_snps.vcf -noDepth \
    -FilterSiteBuilderPlugin \
    -bedFile ./hare_v5_intervals.bed \
    -endPlugin \
    -export /workdir/ag2484/filter_snps1/filtered_hare_nam_phg_snps.vcf \
    -exportType VCF

/workdir/ag2484/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/ag2484/goodman_hare_downsample_debug.log \
    -Xmx1000g \
    -maxThreads 86 \
    -importGuess /workdir/ag2484/goodman_phg_snps.vcf -noDepth \
    -FilterSiteBuilderPlugin \
    -bedFile ./hare_v5_intervals.bed \
    -endPlugin \
    -export /workdir/ag2484/filter_snps1/filtered_hare_goodman_phg_snps.vcf \
    -exportType VCF

# -----------------------------------------
#  filter HARE regions for allele frequency, remove indels,
# -----------------------------------------

/workdir/ag2484/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/ag2484/filter_snps1/goodman_hare_snp_filter.log \
    -Xmx500g \
    -maxThreads 60 \
    -importGuess /workdir/ag2484/filter_snps1/filtered_hare_goodman_phg_snps.vcf -noDepth \
    -FilterSiteBuilderPlugin \
    -siteMinAlleleFreq 0.05 \
    -siteMaxAlleleFreq 0.95 \
    -removeSitesWithIndels true \
    -endPlugin \
    -export /workdir/ag2484/filter_snps1/filtered005_hare_goodman_snps.vcf \
    -exportType VCF

bgzip --threads 60 /workdir/ag2484/filter_snps1/filtered005_hare_goodman_snps.vcf

/workdir/ag2484/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/ag2484/filter_snps1/nam_hare_snp_filter.log \
    -Xmx500g \
    -maxThreads 60 \
    -importGuess /workdir/ag2484/filter_snps1/filtered_hare_nam_phg_snps.vcf -noDepth \
    -FilterSiteBuilderPlugin \
    -siteMinAlleleFreq 0.05 \
    -siteMaxAlleleFreq 0.95 \
    -removeSitesWithIndels true \
    -endPlugin \
    -export /workdir/ag2484/filter_snps1/filtered005_hare_nam_snps.vcf \
    -exportType VCF

bgzip --threads 60 /workdir/ag2484/filter_snps1/filtered005_hare_nam_snps.vcf

# -----------------------------------------
# filter vcf for shared sites in nam and goodman
# combine nam and goodman and prune for ld
# -----------------------------------------

#index the files

bcftools index /workdir/ag2484/filter_snps1/filtered005_hare_goodman_snps1.vcf.gz
bcftools index /workdir/ag2484/filter_snps1/filtered005_hare_nam_snps.vcf1.gz

#get shared sites

bcftools isec -n=2 -c all filtered005_hare_goodman_snps1.vcf.gz filtered005_hare_nam_snps1.vcf.gz > sharedPositions.txt

#filter for shared sites
bcftools view -T sharedPositions.txt filtered005_hare_goodman_snps1.vcf.gz -Oz > filtered005_hare_goodman_sharedPositions.vcf.gz
bcftools view -T sharedPositions.txt filtered005_hare_nam_snps1.vcf.gz -Oz > filtered005_hare_nam_sharedPositions.vcf.gz

#combine two vcfs
bcftools index filtered005_hare_nam_sharedPositions.vcf.gz
bcftools index filtered005_hare_goodman_sharedPositions.vcf.gz

bcftools merge filtered005_hare_nam_sharedPositions.vcf.gz filtered005_hare_goodman_sharedPositions.vcf.gz > \
filtered005_hare_nam_goodman_sharedPositions.vcf
