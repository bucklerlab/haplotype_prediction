time /workdir/ag2484/tassel-5-standalone/run_pipeline.pl -debug -Xmx400G -configParameters /workdir/ag2484/phg_282/phg_config_file_282GBS.txt \
            -HaplotypeGraphBuilderPlugin \
                -configFile /workdir/ag2484/phg_282/phg_config_file_282GBS.txt \
                -methods mummer4 \
                -includeVariantContexts true \
                -endPlugin \
			-ImportHaplotypePathFilePlugin \
				-pathMethodName gbs_282_path_minReads0_minTaxa10_splitProb09999_minTransProb35em6_try2 -endPlugin \
            -PathsToVCFPlugin \
				-outputFile goodman_phg_snps.txt \
				-endPlugin > /workdir/ag2484/phg_282_log.txt