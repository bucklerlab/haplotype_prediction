
#------------------------------------------------------- 
# packages required
#-------------------------------------------------------

library(stringr)
library(ggplot2)

#------------------------------------------------------- 
# directories
#-------------------------------------------------------

snp_dir <- "/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/SNP_prediction/results"
hare_dir <- "/Volumes/bucklerlab_backup/haplotype_prediction/HARE_redone/results/sig_test_result_acrosspop/summary_table_each_tissue"

#Files to rename X-axis trait labels
traits_rename <- read.csv('/Users/ag2484-admin/Dropbox/bucklerlab/haplotype_prediction/haplotype_prediction/HARE_redone/list_of_traits.csv')

#------------------------------------------------------- 
# Plot prediction using snps: trainNAM_testGoodman and trainGoodman_testNAM
#-------------------------------------------------------

trainNAM_testGoodman <- read.csv(paste0(snp_dir, "/prediction_accuracy_trainNAM_test282_snps.csv"), col.names = c("traits", "trainNAM_testGoodman"))

trainGoodman_testNAM <- read.csv(paste0(snp_dir, "/prediction_accuracy_train282_testNAM_snps.csv"), col.names = c("traits", "trainGoodman_testNAM"))

data_snps <- merge(trainNAM_testGoodman, trainGoodman_testNAM, by = "traits")

data_snps.long <- gather(data_snps, key = "Population", value = "prediction_accuracy", 2:3)
data_snps.long$traits <- str_split_fixed(data_snps.long$traits, "_BLUP", 2)[, 1]

plot_snps <- ggplot() + 
  geom_point(data = data_snps.long, aes(x = traits, y = prediction_accuracy, color = Population, shape = Population) , size = 4) +
  theme_bw() +
  labs(y = "Prediction accuracy", x = "Traits") +
  theme(
    axis.title.x = element_text(color = "black", size = 14, face = "bold"), 
    axis.title.y = element_text(color = "black", size = 14, face = "bold"),
    axis.text.y = element_text(color = "grey20", size = 10, face = "plain"),
    axis.text.x = element_text(color = "grey20", size = 10, angle = 30, hjust = 1, vjust = 1, face = "plain"),
    legend.title = element_blank(),
    legend.text = element_text(size = 14, face = "bold"), 
    legend.position = "top",
    ) +
  scale_x_discrete(breaks=traits_rename$Trait_name,
                   labels=traits_rename$Trait_rename) 

ggsave(paste0(snp_dir, "/pred_accuracy_across_pop_snps.png"), plot_snps, width = 10, height = 6, dpi = 'retina' )

#------------------------------------------------------- 
# Train Goodman and test NAM
#-------------------------------------------------------
# prediction using hare(max across tissues)+snps and snps
hare_snps <- read.csv(paste0(snp_dir, "/prediction_accuracy_train282_testNAM_snps_hare.csv"), col.names = c("traits", "HARE+SNPs"))
snps <- read.csv(paste0(snp_dir, "/prediction_accuracy_train282_testNAM_snps.csv"), col.names = c("traits", "SNPs"))

data_all <- merge(hare_snps, snps, by = "traits")
data_all.long <- gather(data_all, key = "Input", value = "prediction_accuracy", 2:3)
data_all.long$traits <- str_split_fixed(data_all.long$traits, "_BLUP", 2)[, 1]

plot_hare_snps_train282_testNAM <- ggplot() + 
  geom_point(data = data_all.long, aes(x = traits, y = prediction_accuracy, color = Input, shape = Input) , size = 4) +
  theme_bw() +
  labs(y = "Prediction accuracy", x = "Traits") +
  theme(
    axis.title.x = element_text(color = "black", size = 14, face = "bold"), 
    axis.title.y = element_text(color = "black", size = 14, face = "bold"),
    axis.text.y = element_text(color = "grey20", size = 10, face = "plain"),
    axis.text.x = element_text(color = "grey20", size = 10, angle = 30, hjust = 1, vjust = 1, face = "plain"),
    legend.title = element_text(colour = "black", size = 18, face = "bold"), 
    legend.text = element_text(size = 14, face = "bold"), 
    legend.justification = "top" ) +
  scale_x_discrete(breaks=traits_rename$Trait_name,
                   labels=traits_rename$Trait_rename) 

ggsave(paste0(snp_dir, "/pred_accuracy_train282_testNAM_hare+snps.png"), plot_hare_snps_train282_testNAM, width = 10, height = 6, dpi = 'retina' )

#prediction using hare (boxplots) & snps

hare_all <- read.csv(paste0(hare_dir, "/alltraits_significance_trainAss282_testNAM.csv"))

data_hare <- hare_all[, c("trait", "imp_acc")]
colnames(data_hare)[which(names(data_hare) == "imp_acc")] <- "prediction_accuracy"
colnames(data_hare)[which(names(data_hare) == "trait")] <- "traits"
data_hare$Input <- "HARE"

data_snps <- read.csv(paste0(snp_dir, "/prediction_accuracy_train282_testNAM_snps.csv"), col.names = c("traits", "SNPs"))

data_snps$traits <- str_split_fixed(data_snps$traits, "_BLUP", 2)[, 1]
data_snps$Input <- "SNPs"
colnames(data_snps)[which(names(data_snps) == "SNPs")] <- "prediction_accuracy"

data_all <- rbind(data_hare, data_snps)

train282 <- ggplot() +
  geom_boxplot(data = data_hare, aes(x = traits, y = prediction_accuracy, fill = Input), outlier.shape = NA) +
  geom_point(data = data_snps, aes(x = traits, y = prediction_accuracy, color = Input), size = 2.5) +
  theme_bw() +
  labs(y = "Prediction accuracy", x = "Traits", title = "Train goodman test NAM") +
  theme(
    axis.title = element_text(color = "black", size = 14, face = "bold"), 
    axis.text.y = element_text(color = "grey20", size = 10, face = "plain"),
    axis.text.x = element_text(color = "grey20", size = 10, angle = 30, hjust = 1, vjust = 1, face = "plain"),
    legend.title = element_blank(),
    legend.position = "top"
  ) +
  scale_fill_manual(values=c("#999999") ) + 
  scale_color_manual(values = "blue") +
  scale_x_discrete(breaks=traits_rename$Trait_name,
                   labels=traits_rename$Trait_rename) 

ggsave(paste0(snp_dir, "/pred_acc_train282_testNAM_snp_hare.png"), train282, width = 10, height = 6, dpi = 'retina' )

#------------------------------------------------------- 
# TrainNAM and test 282
#-------------------------------------------------------

# prediction using hare(max across tissues)+snps and snps
hare_snps <- read.csv(paste0(snp_dir, "/prediction_accuracy_trainNAM_test282_snps_hare.csv"), col.names = c("traits", "HARE+SNPs"))
snps <- read.csv(paste0(snp_dir, "/prediction_accuracy_trainNAM_test282_snps.csv"), col.names = c("traits", "SNPs"))

data_all <- merge(hare_snps, snps, by = "traits")
data_all.long <- gather(data_all, key = "Input", value = "prediction_accuracy", 2:3)
data_all.long$traits <- str_split_fixed(data_all.long$traits, "_BLUP", 2)[, 1]

plot_hare_snps_trainNAM_test282 <- ggplot() + 
  geom_point(data = data_all.long, aes(x = traits, y = prediction_accuracy, color = Input, shape = Input) , size = 4) +
  theme_bw() +
  labs(y = "Prediction accuracy", x = "Traits") +
  theme(
    axis.title.x = element_text(color = "black", size = 14, face = "bold"), 
    axis.title.y = element_text(color = "black", size = 14, face = "bold"),
    axis.text.y = element_text(color = "grey20", size = 10, face = "plain"),
    axis.text.x = element_text(color = "grey20", size = 10, angle = 30, hjust = 1, vjust = 1, face = "plain"),
    legend.title = element_text(colour = "black", size = 18, face = "bold"), 
    legend.text = element_text(size = 14, face = "bold"), 
    legend.justification = "top" ) +
  scale_x_discrete(breaks=traits_rename$Trait_name,
                   labels=traits_rename$Trait_rename) 

ggsave(paste0(snp_dir, "/pred_accuracy_trainNAM_test282_hare+snps.png"), plot_hare_snps_trainNAM_test282 , width = 10, height = 6, dpi = 'retina' )


#prediction using hare (boxplots) & snps

hare_all <- read.csv(paste0(hare_dir, "/alltraits_significance_trainNAM_testAss282.csv"))

data_hare <- hare_all[, c("trait", "imp_acc")]
colnames(data_hare)[which(names(data_hare) == "imp_acc")] <- "prediction_accuracy"
colnames(data_hare)[which(names(data_hare) == "trait")] <- "traits"
data_hare$Input <- "HARE"

data_snps <- read.csv(paste0(snp_dir, "/prediction_accuracy_trainNAM_test282_snps.csv"), col.names = c("traits", "SNPs"))

data_snps$traits <- str_split_fixed(data_snps$traits, "_BLUP", 2)[, 1]
data_snps$Input <- "SNPs"
colnames(data_snps)[which(names(data_snps) == "SNPs")] <- "prediction_accuracy"

data_all <- rbind(data_hare, data_snps)

trainNAM <- ggplot() +
  geom_boxplot(data = data_hare, aes(x = traits, y = prediction_accuracy, fill = Input), outlier.shape = NA) +
  geom_point(data = data_snps, aes(x = traits, y = prediction_accuracy, color = Input), size = 2.5) +
  theme_bw() +
  labs(y = "Prediction accuracy", x = "Traits", title = "Train NAM test goodman") +
  theme(
    axis.title = element_text(color = "black", size = 14, face = "bold"), 
    axis.text.y = element_text(color = "grey20", size = 10, face = "plain"),
    axis.text.x = element_text(color = "grey20", size = 10, angle = 30, hjust = 1, vjust = 1, face = "plain"),
    legend.title = element_blank(),
    legend.position = "top"
  ) +
  scale_fill_manual(values=c("#999999") ) + 
  scale_color_manual(values = "blue") +
  scale_x_discrete(breaks=traits_rename$Trait_name,
                   labels=traits_rename$Trait_rename) 

ggsave(paste0(snp_dir, "/pred_acc_trainNAM_test282_snp_hare.png"), trainNAM, width = 10, height = 6, dpi = 'retina' )



