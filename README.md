Haplotype Associated RNA Expression (HARE) in Prediction of Complex Traits in Maize

This directory contains all scripts from study titled "Haplotype Associated RNA Expression (HARE) Improves Prediction of Complex Traits in Maize".

Repository Contents:

HARE_redone/src: Includes all script for 
	(i) Getting haplotypes in NAM and Goodman Association panels from the PHG database (Valdes Franco et al., 2020)
	(ii) imputing HARE in NAM and Goodman Association panels based on the NAM founders haplotypes and expression in Goodman panel (Kremling et al, 2018)
	(ii) prediction models within the population in Goodman panel or across population in NAM and Goodman panels using HARE and random HARE. 

HARE_redone/src/plots_for_manuscripts: Includes script to generate plots for the manuscript, plot numbers are labelled as they appear on the manuscript


SNP_Prediction: Includes all script for 
	(i) getting SNPs in two populations
	(ii) genomic prediction using SNPs
	(iii) plots for manuscripts

General info on scripts:
All analysis scripts are numbered based on the sequence they were run. 

Input files:
	PHG database (Franco et al., 2020) or the haplotype matrix and SNPs stored in dryad data repository (doi: will be updated later)
	Expression matrix for 7 tissues in Goodman panel (Kremling et al., 2018)
	Key to convert v3 <-> v5 in HARE_redone/data
	Phenotypes to predict are in HARE_redone/data/phenotypes (collected from multiple studies)


Manuscript: 
BioRxiv version: Giri, A., Khaipho-Burch, M., Buckler, E. S., & Ramstein, G. P. (2021). Haplotype Associated RNA Expression (HARE) Improves Prediction of Complex Traits in Maize. bioRxiv.

**Contact**
Primary Contact: Anju Giri (ag2484@cornell.edu)
Other Contacts: Cinta Romay (mcr72@cornell.edu), Sara Miller (sara.miller@cornell.edu), Ed Buckler (esb33@cornell.edu)